The library was written according to the instructions in this book:
http://www.zemris.fer.hr/predmeti/irg/labosi/B_Labosi.pdf

The book was written by Marko Čupić and is used for the FER 3rd year course
Interactive Computer Graphics in the Laboratory Assignments version B.
The chapter that covers and provides the resources and guidelines for
developing this library is Chapter 1: "Izrada pomoćne biblioteke".

There are 2 versions of Laboratories - A and B. A is the shorter and the easier
one, while B is the longer and the harder one. I've done the A version, and in
addition to it I've also done this linear algebra mathematics library from the
B version.

I've done some testing and playing around in the VectorsSandbox.cpp and the
main.cpp and Tests.h files.
However, I can definitely see some improvements that can yet be made:
		- Add smart pointers
		- Test some more
		- Optimise
		- GUI
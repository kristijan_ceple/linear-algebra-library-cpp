#pragma once
#include "AbstractMatrix.h"
#include "IMatrix.h"

// Vector as matrix ehh lad
class MatrixVectorView :
	public AbstractMatrix
{
private:
	bool _asRowMatrix;
	IVector* _original;
	IMatrix* _mat;
	MatrixVectorView(bool, IVector*, IMatrix*);
public:
	MatrixVectorView(IVector* original, bool asRowMatrix);
	size_t getRowsCount();
	size_t getColsCount();
	double get(size_t, size_t);
	IMatrix* set(size_t, size_t, double);
	IMatrix* copy();
	IMatrix* newInstance(size_t, size_t);
};


#include "AbstractMatrix.h"

#pragma once
#ifndef MATRIX_H
#define MATRIX_H

class Matrix :
	public AbstractMatrix
{
protected:
	double** _elements;
	size_t _rows;
	size_t _cols;

public:
	Matrix(size_t, size_t);
	Matrix(size_t rows, size_t cols, double**, bool);
	size_t getRowsCount();
	size_t getColsCount();
	double get(size_t, size_t);
	IMatrix* set(size_t, size_t, double);
	IMatrix* copy();
	IMatrix* newInstance(size_t, size_t);
	static IMatrix* parseSimple(std::string);
	~Matrix();
};

#endif


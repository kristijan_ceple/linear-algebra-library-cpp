#include "MatrixVectorView.h"
#include "Matrix.h"
#include "IVector.h"
#include <iostream>

MatrixVectorView::MatrixVectorView(IVector* original, bool asRowMatrix)
{
	this->_original = original;
	this->_asRowMatrix = asRowMatrix;
	
	double** newArr;
	if (asRowMatrix) {
		double* vectArr = original->toArray();
		newArr = new double*;
		size_t elNum = _original->getDimension();

		*newArr = new double[elNum];
		memcpy(newArr[0], vectArr, sizeof(vectArr[0]) * elNum);
		this->_mat = new Matrix{ 1, original->getDimension(), newArr, true };
	}
	else {
		// multiple rows, but one column
		double* vectArr = original->toArray();
		size_t elNum = _original->getDimension();
		newArr = new double*[elNum];
		for (int i = 0; i < elNum; i++) {
			newArr[i][0] = vectArr[i];
		}

		this->_mat = new Matrix{ original->getDimension(), 1, newArr, true };
	}
}

size_t MatrixVectorView::getRowsCount()
{
	return _mat->getRowsCount();
}

size_t MatrixVectorView::getColsCount()
{
	return _mat->getColsCount();
}

double MatrixVectorView::get(size_t row, size_t col)
{
	return this->_mat->get(row, col);
}

IMatrix* MatrixVectorView::set(size_t row, size_t col, double toSetVal)
{
	this->_mat->set(row, col, toSetVal);

	// Need to change the original vector as well
	size_t toSetIndex = _asRowMatrix ? col : row;
	this->_original->set(toSetIndex, toSetVal);

	return this;
}

IMatrix* MatrixVectorView::copy()
{
	// COpy the matrix first
	IVector* newVect = this->_original->copy();
	IMatrix* toReturn = new MatrixVectorView{ newVect, this->_asRowMatrix };
	return toReturn;
}

IMatrix* MatrixVectorView::newInstance(size_t rows, size_t cols)
{
	if (rows <= 0 || cols <= 0) {
		throw std::out_of_range("Index or indice <= 0 exception! Will not create a new instance!");
	}
	
	IVector* newVect = this->_original->copy();
	
	size_t copyIndex = _asRowMatrix ? cols : rows;
	size_t origSizeIndex = _asRowMatrix ? this->_mat->getColsCount() : this->_mat->getRowsCount();
	origSizeIndex--;

	for (int i = copyIndex; i < origSizeIndex; i++) {
		newVect->set(i, 0);
	}

	IMatrix * toReturn = new MatrixVectorView{ newVect, this->_asRowMatrix };
}
#include "MatrixSubMatrixView.h"
#include <iostream>

MatrixSubMatrixView::MatrixSubMatrixView(IMatrix* origMat, size_t row, size_t col)
{
	this->_origMat = origMat;
	this->_rowsNum = origMat->getRowsCount() - 1;
	this->_colsNum = origMat->getColsCount() - 1;

	this->_rowIndice = new size_t[_rowsNum];
	this->_colIndice = new size_t[_colsNum];

	// Need to set the indice now
	for (size_t i = 0, localArrIndex = 0; i < origMat->getRowsCount(); i++) {
		if (i != row) {
			this->_rowIndice[localArrIndex++] = i;
		}
	}

	for (size_t j = 0, localArrIndex = 0; j < origMat->getColsCount(); j++) {
		if (j != col) {
			this->_colIndice[localArrIndex++] = j;
		}
	}
}

MatrixSubMatrixView::MatrixSubMatrixView(IMatrix* origMat, size_t* rowsArr, size_t numRows, size_t* colsArr, size_t numCols)
{
	this->_origMat = origMat;

	this->_rowIndice = rowsArr;
	this->_rowsNum = numRows;

	this->_colIndice = colsArr;
	this->_colsNum = numRows;
}

size_t MatrixSubMatrixView::getRowsCount()
{
	return this->_rowsNum;
}

size_t MatrixSubMatrixView::getColsCount()
{
	return this->_colsNum;
}

double MatrixSubMatrixView::get(size_t row, size_t col)
{
	// First check the args
	if (col >= this->_colsNum || row >= this->_rowsNum || row < 0 || col < 0) {
		throw std::out_of_range("Index out of range access!");
	}

	// Need to find the element at row, and col
	return this->_origMat->get(this->_rowIndice[row],
		this->_colIndice[col]);
}

IMatrix* MatrixSubMatrixView::set(size_t row, size_t col, double toSetVal)
{
	// Check args

	this->_origMat->set(this->_rowIndice[row], this->_colIndice[col], toSetVal);
	return this;
}

IMatrix* MatrixSubMatrixView::copy()
{
	// Just do the same
	IMatrix* toReturn = new MatrixSubMatrixView{this->_origMat, this->_rowIndice, this->_rowsNum, this->_colIndice, this->_colsNum};
	return toReturn;
}

IMatrix* MatrixSubMatrixView::newInstance(size_t rows, size_t cols)
{
	// Will have to do some magic 'ere
	/*
	Let's think about the easiest way of doing this:
	I can get the new indice into separate arrays. Then the rest is easy!!!
	*/

	size_t* newRowIndice = new size_t[rows];
	size_t* newColIndice = new size_t[cols];

	memcpy(newRowIndice, _rowIndice, rows * sizeof(_rowIndice));
	memcpy(newColIndice, _colIndice, cols * sizeof(_colIndice));

	return new MatrixSubMatrixView{this->_origMat, this->_rowIndice, this->_rowsNum, this->_colIndice, this->_colsNum};
}

IMatrix* MatrixSubMatrixView::subMatrix(size_t row, size_t col, bool liveView)
{
	// We have to hide row and col
	// Oh fuck
	IMatrix* newMat;
	if (liveView) {
		// Return another instance of me
		newMat = this->_origMat;
	}
	else {
		// Return another instance of me in a copy
		newMat = this->_origMat->copy();
	}

	// Now, we have to do the for loops while forgetting a row and a 

	size_t* newRows= new size_t[this->_rowsNum];
	size_t* newCols = new size_t[this->_colsNum];

	for (size_t i = 0, localArrIndex = 0; i < this->_rowsNum; i++) {
		if (i != row) {
			newRows[localArrIndex++] = this->_rowIndice[i];
		}
		else {
			// Skip this one
			continue;
		}
	}

	for (size_t j = 0, localArrIndex = 0; j < this->_colsNum; j++) {
		if (j != col) {
			newCols[localArrIndex++] = this->_colIndice[j];
		}
		else {
			continue;
		}
	}

	// Now make a new class with the same matrix, BUT new and differend fields!
	IMatrix* toReturn = new MatrixSubMatrixView{ newMat, newRows, this->_rowsNum - 1, newCols, this->_colsNum - 1 };
	return toReturn;
}
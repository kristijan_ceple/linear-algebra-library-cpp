#include "IMatrix.h"

#pragma once
#ifndef ABSTRACTMATRIX_H
#define ABSTRACTMATRIX_H

class AbstractMatrix : public IMatrix
{
public:
	IMatrix* nTranspose(bool);
	IMatrix* add(IMatrix&);
	IMatrix* nAdd(IMatrix&);
	IMatrix* sub(IMatrix&);
	IMatrix* nSub(IMatrix&);
	IMatrix* nMultiply(IMatrix&);
	double determinant();
	IMatrix* subMatrix(size_t, size_t, bool);
	virtual IMatrix* scalarMultiply(double, bool);
	IMatrix* nInvert();
	double** toArray();
	std::string toString(size_t);
	double multiplyRowColumn(IMatrix& otherMat, size_t currRow, size_t currCol, size_t firstCols, size_t secondRows);
	IVector* toVector(bool);
};

#endif

